================
Carrier load UL
================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from proteus import Model, Wizard
    >>> today = datetime.date.today()

Install agro Module::

    >>> config = activate_modules('carrier_load_done2running')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.account_category = account_category_tax
    >>> carrier_product.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()

Create carrier load::

    >>> Load = Model.get('carrier.load')
    >>> load = Load()
    >>> load.company != None
    True
    >>> load.state
    'draft'
    >>> load.date == today
    True
    >>> load.warehouse != None
    True
    >>> load.warehouse_output == load.warehouse.output_location
    True
    >>> load.dock != None
    True
    >>> load.carrier = carrier
    >>> load.vehicle_number = 'MX4459'
    >>> load.save()
    >>> load.code != None
    True

Create load order::

    >>> Order = Model.get('carrier.load.order')
    >>> order = Order(load=load)
    >>> order.start_date = datetime.datetime.now() - relativedelta(minutes=20)
    >>> order.state
    'draft'
    >>> order.ul_origin_restrict
    True
    >>> order.party = customer
    >>> line = order.lines.new()
    >>> line.ul_quantity = Decimal(1)
    >>> order.save()
    >>> order.code != None
    True
    >>> order.date == load.date
    True
    >>> order.click('wait')
    >>> order.state
    'waiting'

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> ul = create_unit_load(config=config, do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> template = ul.product.template
    >>> template.salable = True
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> main_product = ul.product

Add other products to unit load::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Plastic Case 30x30'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> product.save()
    >>> move = ul.moves.new()
    >>> move.planned_date = today
    >>> move.product = product
    >>> move.quantity = Decimal(2)
    >>> move.from_location = ul.moves[0].from_location
    >>> move.to_location = ul.moves[0].to_location
    >>> move.currency = move.company.currency
    >>> move.unit_price = product.cost_price
    >>> ul.save()

Starting load wizard::

    >>> start_load = Wizard('carrier.load_uls', [])
    >>> start_load.form.load_order = order
    >>> start_load.execute('post_order')
    >>> start_load.form.load_order == order
    True
    >>> len(start_load.form.uls_loaded)
    0
    >>> start_load.form.loaded_uls
    0
    >>> start_load.form.ul_code = 'X'
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Cannot find Unit load "X". - 

Check UL loading restrictions::

    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "1" must be in Done state. - 
    >>> ul.click('assign')
    >>> ul.click('do')
    >>> start_load.execute('load_')
    >>> start_load.form.loaded_uls
    1
    >>> order.reload()
    >>> order.state
    'running'
    >>> order.start_date != None
    True
    >>> len(order.unit_loads)
    1
    >>> start_load.form.ul_code = ul.code
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: UL "1" is already loaded. - 


Add an invalid UL::

    >>> Location = Model.get('stock.location')
    >>> ul2 = create_unit_load(do_state=False,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    >>> ul2.save()
    >>> ul2.click('do')
    >>> start_load.form.ul_code = ul2.code
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='loading_ul_origin_2', always=True).save()
    >>> start_load.execute('load_')
    Traceback (most recent call last):
        ...
    trytond.modules.carrier_load_ul.exceptions.AddUnitLoadOverloadError: All valid lines of load order "1" are complete. Cannot load more ULs. - 

Unload UL::

    >>> order.lines[0].ul_quantity += 1
    >>> order.save()
    >>> start_load.execute('load_')
    >>> start_load.form.loaded_uls
    2
    >>> ul2 = Unitload(ul2.id)
    >>> start_load.form.uls_loaded.append(ul2)
    >>> start_load.execute('unload_')
    >>> start_load.form.loaded_uls
    1
    >>> ul2.load_line == None
    True
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.form.ul_code = ul2.code
    >>> start_load.execute('load_')
    >>> start_load.execute('exit')
    >>> order.reload()
    >>> len(order.unit_loads)
    2
    >>> ul2.click('unload')
    >>> order.reload()
    >>> len(order.unit_loads)
    1

Finish loading::

    >>> start_load = Wizard('carrier.load_uls', [order])
    >>> start_load.execute('pre_do')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserWarning: You have loaded less ULs (1) than expected (2). - 
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='pending_uls_1', always=True).save()
    >>> start_load.execute('pre_do')
    >>> order.reload()
    >>> len(order.unit_loads)
    1
    >>> order.state
    'done'

Check sale::

    >>> order.sale != None
    True
    >>> order.sale.state
    'quotation'
    >>> order.sale.number != None
    True
    >>> len(order.sale.lines)
    1
    >>> order.sale.lines[0].product.id == ul.product.id
    True
    >>> len(order.sale.shipments)
    1
    >>> shipment, = order.sale.shipments
    >>> shipment.start_date == order.start_date
    True
    >>> shipment.end_date == order.end_date
    True
    >>> len(shipment.outgoing_moves)
    2
    >>> shipment.outgoing_moves[0].unit_load.id == ul.id
    True
    >>> len(shipment.inventory_moves)
    2
    >>> shipment.inventory_moves[0].unit_load.id == ul.id
    True
    >>> len(order.inventory_moves)
    0
    >>> inv_move, = [m for m in shipment.inventory_moves if m.product != ul.product]    
    >>> inv_move.product.rec_name
    'Plastic Case 30x30'
    >>> inv_move.quantity
    2.0
    >>> inv_move.start_date == order.start_date
    True
    >>> inv_move.end_date == order.end_date
    True
    >>> len(order.outgoing_moves)
    1
    >>> order.outgoing_moves[0].product.rec_name
    'Plastic Case 30x30'
    >>> order.outgoing_moves[0].quantity
    2.0

Undo Sale and Shipment::

    >>> Sale = Model.get('sale.sale')
    >>> Shipment = Model.get('stock.shipment.out')
    >>> Order = Model.get('carrier.load.order')
    >>> sale_id, sale_number = order.sale.id, order.sale.number
    >>> shipment_id, shipment_number = order.shipment.id, order.shipment.number
    >>> order.sale.state
    'quotation'
    >>> order.state
    'done'
    >>> order.shipment.state
    'packed'
    >>> len(order.sale.lines)
    1
    >>> len(order.shipment.outgoing_moves)
    2
    >>> len(order.shipment.inventory_moves)
    2
    >>> order.click('run')
    >>> order.reload()
    >>> order.sale.state
    'cancelled'
    >>> order.shipment.state
    'cancelled'
    >>> not order.inventory_moves
    True
    >>> not order.outgoing_moves
    True
    >>> order.state
    'running'
    >>> len(order.sale.lines)
    0
    >>> len(order.shipment.moves)
    0
    >>> order.click('do')
    >>> order.reload()
    >>> len(order.sale.lines)
    1
    >>> order.shipment.state
    'packed'
    >>> len(order.shipment.moves)
    4
    >>> sale_id != order.sale.id
    True
    >>> sale_number == order.sale.number
    True
    >>> shipment_id != order.shipment.id
    True
    >>> shipment_number == order.shipment.number
    True
    >>> order.state
    'done'
    >>> len(Sale.find([]))
    1
    >>> len(Shipment.find([]))
    1
    >>> order.click('run')
    >>> ul ,= order.unit_loads
    >>> ul.click('unload')
    >>> for st in ['wait', 'draft', 'cancel']:
    ...     order.click(st)
    >>> order.delete()
    >>> len(Sale.find([]))
    0
    >>> len(Shipment.find([]))
    0

Create internal load to check later moves with success::

    >>> wh, = Location.find([
    ...     ('code', '=', 'WH')], limit=1)
    >>> wh2, = wh.duplicate()
    >>> wh2.code = 'WH2'
    >>> wh2.save()

    >>> load2 = Load()
    >>> load2.carrier = carrier
    >>> load2.warehouse = wh
    >>> load2.vehicle_number = 'MX4459'
    >>> load2.save()
    >>> load_order = Order(load=load2)
    >>> load_order.type = 'internal'
    >>> load_order.to_location = wh2.storage_location
    >>> load_order.ul_origin_restrict = False
    >>> load_order_line = load_order.lines.new()
    >>> load_order_line.ul_quantity = Decimal(1)
    >>> load_order.click('wait')

    >>> unit_load = create_unit_load(config=config,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})

    >>> start_load = Wizard('carrier.load_uls', [load_order])
    >>> start_load.form.ul_code = unit_load.code
    >>> start_load.execute('load_')
    >>> start_load.execute('pre_do')
    >>> load_order.reload()
    >>> len(load_order.unit_loads)
    1
    >>> load_order.state
    'done'
    >>> load_order.click('run')
    >>> load_order.state
    'running'

Create internal load to check later moves with failure::

    >>> load3 = Load()
    >>> load3.carrier = carrier
    >>> load3.warehouse = wh
    >>> load3.vehicle_number = 'MX4459'
    >>> load3.save()
    >>> load_order2 = Order(load=load2)
    >>> load_order2.type = 'internal'
    >>> load_order2.start_date = datetime.datetime.now() - relativedelta(minutes=30)
    >>> load_order2.end_date = datetime.datetime.now() - relativedelta(minutes=15)
    >>> load_order2.to_location = wh2.storage_location
    >>> load_order2.ul_origin_restrict = False
    >>> load_order_line2 = load_order2.lines.new()
    >>> load_order_line2.ul_quantity = Decimal(1)
    >>> load_order2.click('wait')

    >>> unit_load2 = create_unit_load(config=config,
    ...         default_values={'start_date': datetime.datetime.now() - relativedelta(days=1)})
    >>> unit_load2.at_warehouse == wh
    True

    >>> start_load = Wizard('carrier.load_uls', [load_order2])
    >>> start_load.form.ul_code = unit_load2.code
    >>> start_load.execute('load_')
    >>> start_load.execute('pre_do')
    >>> load_order2.reload()
    >>> len(load_order2.unit_loads)
    1
    >>> load_order2.state
    'done'
    >>> load_order2.shipment.state
    'done'
    >>> unit_load2.reload()
    >>> unit_load2.at_warehouse == wh2
    True
    >>> load_order2.click('run')
    >>> load_order2.shipment.state
    'cancelled'
    >>> unit_load2.reload()
    >>> unit_load2.at_warehouse == wh
    True
    >>> load_order2.click('do')
    >>> load_order2.shipment.state
    'done'
    >>> unit_load2.reload()
    >>> unit_load2.at_warehouse == wh2
    True

Add moves to Unit Load::

    >>> move_try = Wizard('stock.unit_load.do_move', [unit_load2])
    >>> move_try.form.location = wh.storage_location
    >>> move_try.execute('move_')

Back to run load Order2::

    >>> load_order2.reload()
    >>> load_order2.click('run') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Cannot move unit load "..." at date "..." because later moves exist. - 
